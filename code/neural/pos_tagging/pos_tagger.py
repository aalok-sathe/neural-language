###############################################################################
####	Pramble    ############################################################
print("Section: preamble")

import keras as K
from keras import layers as kl
import numpy

import os
import io
#import progressbar

import time
import datetime

import pickle


################################################################################
####    Define and prepare input    ############################################
print("Section: define and prepare input")

max_docs = 10
base_path = "../../../data/spacy-tagged"
# os.listdir(base_path)
static_files_list = pickle.load(open('files_list.pickle', 'rb'))

# vocab = set()
# token_to_index = dict()
# index_to_token = dict()

# pos_tags = set()
pos_tags = pickle.load(open('dictionaries/pos_tags_2018-06-07_19-54.pickle', 'rb'))
# tag_to_index = dict()
tag_to_index = pickle.load(open('dictionaries/tag_to_index_2018-06-07_19-54.pickle', 'rb'))
# index_to_tag = dict()
index_to_tag = pickle.load(open('dictionaries/index_to_tag_2018-06-07_19-54.pickle', 'rb'))

# for fname in static_files_list[:max_docs]:
#     for line in io.open(os.path.join(base_path, fname), "r", errors = 'ignore'):
#         splitline = line.lower().split()
#         if len(line) <= 1 or len(splitline) <= 2:
#             continue
#         vocab.update(splitline[0:1])
#         pos_tags.update(splitline[1:2])

# index = 1
# for token in vocab:
#     token_to_index[token] = index
#     index_to_token[index] = token
#     index += 1
#
# index = 0
# for tag in pos_tags:
#     tag_to_index[tag] = index
#     index_to_tag[index] = tag
#     index += 1
#
# print(tag_to_index)

################################################################################
####    Define and compile model    ############################################
print("Section: define and compile model")

model = K.models.Sequential()

#input_layer = kl.Input(shape=(1,))
#model.add(input_layer)

model.add(kl.Embedding(99999, 256, input_length=3))
# model.add(kl.Embedding(len(vocab), 256, input_length=3))
model.add(kl.Flatten())
model.add(kl.Dense(len(pos_tags)*2, activation="relu")) ###
model.add(kl.Dense(len(pos_tags), activation="softmax"))

model.compile(optimizer="adam", loss="categorical_crossentropy", metrics=["categorical_accuracy"])

print(model.summary())
################################################################################
####    Train model by iteratively reading tokens from input
print("Section: train model by iteratively reading tokens from input")

class TwoWayDict(dict):
    def __setitem__(self, key, value):
        # Remove any previous connections with these values
        if key in self:
            del self[key]
        if value in self:
            del self[value]
        dict.__setitem__(self, key, value)
        dict.__setitem__(self, value, key)

    def __delitem__(self, key):
        dict.__delitem__(self, self[key])
        dict.__delitem__(self, key)

    def __len__(self):
        """Returns the number of connections"""
        return dict.__len__(self) // 2

tokens = TwoWayDict()

def token_to_index(token):
    string_of_token = token
    token = token[:5]
    token += ' ' * (5-len(token))
    for iter in range(len(token)):
        token = token[:iter] + str(ord(token[iter]) % 10) + token[iter+1:]
    tokens[string_of_token] = token
    return int(token)
# token_to_index('astonishingly')

def data_generator(start=0,end=max_docs):

    fist_token = 0; first_tag = 0
    second_token = 0; second_tag = 0
    third_token = 0; third_tag = 0

    for fname in static_files_list[start:end]:

        for line in io.open(os.path.join(base_path, fname+".corpus"), "r", errors = 'ignore'):

            splitline = line.lower().split()
            if len(line) <= 1 or len(splitline) <= 2:
                continue

            first_token = second_token
            second_token = third_token
            third_token = token_to_index(splitline[0])

            first_tag = second_tag
            second_tag = third_tag
            third_tag = tag_to_index[splitline[1]]

            # print(
            #     index_to_token.get(first_token,0),
            #     index_to_token.get(second_token,0),
            #     index_to_token.get(third_token,0), "\n",
            #     index_to_tag.get(first_tag,0),
            #     index_to_tag.get(second_tag,0),
            #     index_to_tag.get(third_tag,0), "\n"
            # )

            x_batch = numpy.zeros((1,3))
            x_batch[0] = numpy.array((first_token, second_token, third_token))

            y_batch = numpy.zeros((1,len(pos_tags)))
            y_batch[0,second_tag] = 1

            #print((x_batch, y_batch), (x_batch.shape, y_batch.shape))

            #yield (numpy.zeros((47,12)), numpy.zeros((12,1)))
            yield (x_batch, y_batch)

            #model.fit(x=x_batch, y=y_batch, batch_size=1, epochs=3, verbose=1)

# Train model using generator syntax
model.fit_generator(data_generator(), steps_per_epoch=100, epochs=100)#, verbose=0)

with open('two-way-token:99999.pickle', 'wb') as out:
    pickle.dump(tokens, out)

x_batch = next(data_generator())[0]
print(model.predict(x_batch))

# Save model after done training

timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H-%M')
model.save("model_%s.kerasmodel" %(timestamp))

################################################################################
   # Evaluate model against remaining data
print("Section: evaluate model against remaining data")

print( model.evaluate_generator(data_generator(max_docs, len(static_files_list)), steps=100, workers=1) )
# model = K.Model()
