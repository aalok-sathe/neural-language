# Neural Language Modeling
[![Keras](https://img.shields.io/badge/framework-keras-red.svg)](https://keras.io)
[![Python ver](https://img.shields.io/pypi/pyversions/Django.svg)](https://www.python.org/)

This repository houses several exploratory programs and scripts that use neural
networks and distributed semantics in natural language processing tasks.
Datasets are included for completion, and in compliance with their license;
however, you should ideally obtain you own copy of the [freely available]
datasets before running any of the scripts here.

##  Datasets
In the directory `/data` you will find a corpus of texts from the [Gutenberg
project](https://web.eecs.umich.edu/~lahiri/gutenberg_dataset.html); and a
corpus of Arthur Conan Doyle's very first Sherlock Holmes writings.
In a subdirectory, you will find an annotated variant of the Gutenberg texts,
created using SpaCy (not hand-annotated).

##  Scripts
### POS Tagging
:warning: Requires: `python3.x`
:warning: Depends on: `keras`, `tensorflow`, `numpy`
:warning: Suggested additional dependency: `hypertools`

In the directory `/code/neural/pos_tagging`, you will find several scripts.
* `pos_tagger.py`: this script uses the auto-annotated Gutenberg texts to train
a deep neural network using word embeddings. Evaluated accuracy: 65-70%
* `alph_pos_tagger.py`: this script is adapted from the previous one, but uses
character embeddings instead of word embeddings, thus speeding up the process
and taking less memory.
In the subdirectory `<pos_tagging>/embeddings`, you will find tabularized data,
which are the embeddings weights extracted from various models trained as POS
taggers. These weights are used to visualize a distributed representation of
embedded entities: words and characters. The script `play_with_embeddings.py`
neatly processes such data for a word embeddings weights instance, and plots
it after reducing dimensions, using the `hypertools` library. Several images are
stored which represent different arguments to the plotting function.

#### Sub-script in POS Tagging: *SpaCy POS Tagger* (`/code/neural/spacy-pos-tagger`)
:warning: Depends on: `spacy`

This script is useful to tag a bunch of [raw] text data using the state-of-the-
art SpaCy tagger. Other experimental models to learn how to create POS Taggers
may then be trained on such data.


================
## Repo structure
```
.
├── code
│   ├── neural
│   │   ├── gensim-word2vec
│   │   │   ├── progress_printer.py
│   │   │   ├── tester.py
│   │   │   └── word2vec_using_gensim.py
│   │   ├── keras-polynomials
│   │   │   ├── keras_trial.py
│   │   │   └── pima-diabetes.data.csv
│   │   ├── keras-word-embeddings [7 entries exceeds filelimit, not opening dir]
│   │   ├── pos_tagging [15 entries exceeds filelimit, not opening dir]
│   │   └── spacy-pos-tagger
│   │       ├── pos_tag.py
│   │       └── progress_printer.py
│   └── rule-based
│       ├── EditDistance.py
│       └── progress_printer.py
├── data
│   ├── Gutenberg
│   │   └── txt [3033 entries exceeds filelimit, not opening dir]
│   ├── gutenberg_tagged.tar.gz
│   ├── gutenberg_texts.tar.gz
│   ├── sherlock
│   │   └── sh.txt.corpus
│   ├── sherlock.tar.gz
│   └── spacy-tagged [100 entries exceeds filelimit, not opening dir]
├── LICENSE
├── README.md
├── repo-size-stats.md
└── TODO.md
```