#! /bin/env/ python3

################################################
#   Bunch of import statements
################################################
import pickle               # Storing python objects as binaries
import pandas as pd         # DataFrame management
import numpy as np          # Computations
import hypertools as hyp    # Plotting and dimensionality reduction
import enchant              # Spell-check library

####    Sort out current directory and data directory
import os
os.chdir(os.path.dirname(os.path.abspath('__file__')))
os.getcwd()

# char_embeddings = np.array((128,64))
# with open('ascii_embeddings_64.pickle', 'rb') as file:
#     char_embeddings = np.array(pickle.load(file)[1:129])

####    A custom class to maintain a two-way hashed reference
class TwoWayDict(dict):
    def __setitem__(self, key, value):
        # Remove any previous connections with these values
        if key in self:
            del self[key]
        if value in self:
            del self[value]
        dict.__setitem__(self, key, value)
        dict.__setitem__(self, value, key)

    def __delitem__(self, key):
        dict.__delitem__(self, self[key])
        dict.__delitem__(self, key)

    def __len__(self):
        """Returns the number of connections"""
        return dict.__len__(self) // 2
with open('../two-way-token:99999.pickle', 'rb') as file:
    two_way_tokensdict = pickle.load(file)
# two_way_tokensdict.keys()

############################################################
####    Load the embeddings layer weights from a previously
####    processed Pandas DataFrame, pickled
############################################################
with open('99999word-embedding.pandas.pickle', 'rb') as file:
    data = pickle.load(file)
    # print(data.head())

all = set(str(x) for x in range(99999)) # All numbers till 99999
some = list(all - two_way_tokensdict.keys()) # Numbers we don't want
array_to_int = np.vectorize(lambda x: int(x), otypes=[np.int])
some = array_to_int(some)
# print(99999-len(some))
data = data.drop(data.index[some]) # Exclude row-indices that we don't want

include = all - (all - two_way_tokensdict.keys()) # Numbers that are words
include = array_to_int(list(include))
include.sort()
labels = [] # Prepare labels for use in the plot
for x in include:
    labels.append(two_way_tokensdict[str(x)])


# for idx,x in enumerate(char_embeddings):
#     print (idx,x)#[chr(x) for x in range(128)][idx])
#     break
#
# labels = [chr(x) for x in range(128+1)] + [None for x in range(128)]
#  labels=labels,

########################################################
####    Call the plot function
hyp.plot(data, '.', n_clusters=10, reduce='PCA', labels=labels, explore=True, save_path="word_embeddings_.png")
